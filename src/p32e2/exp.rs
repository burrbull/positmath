use super::*;
use softposit::p32e2;

const HALF: [u32; 2] = [0x_3800_0000, 0x_c800_0000];

const LN2_HI: P32E2 = P32E2::new(0x_3b17_2000); // 0.693145751953125
const LN2_LO: P32E2 = P32E2::new(0x_023f_be8e); // 0.000001428606765330187
const INV_LN2: P32E2 = P32E2::new(0x_438a_a3b3); // 1.4426950439810753
const P1: P32E2 = P32E2::new(0x_2aaa_a8f0); // 1.6666625440120697e-1
const P2: P32E2 = P32E2::new(-0x_0ed5_4854); // -2.7667332906275988e-3
const ZERO: P32E2 = P32E2::new(0);
const ONE: P32E2 = P32E2::new(0x_4000_0000);
const TWO: P32E2 = P32E2::new(0x_4800_0000);

#[inline]
pub fn exp(mut x: P32E2) -> P32E2 {
    if x.is_infinite() {
        return x;
    }

    let mut hx = x.to_bits();
    let sign = (hx >> 31) as i32; /* sign bit of x */
    let signb = sign != 0;
    if signb {
        hx = hx.wrapping_neg()
    }; /* high word of |x| */

    /* special cases */
    if hx >= 0x_6932_d7b3 {
        /* if |x| >= 83.177661667 */
        if !signb {
            return p32e2::INFINITY;
        } else {
            return ZERO;
        }
    }

    /* argument reduction */
    let k: i32;
    let hi: P32E2;
    let lo: P32E2;
    if hx > 0x_3317_217f {
        /* if |x| > 0.5 ln2 */
        if hx > 0x_4051_5920 {
            /* if |x| > 1.5 ln2 */
            k = i32::from(INV_LN2 * x + P32E2::from_bits(HALF[sign as usize]));
        } else {
            k = 1 - sign - sign;
        }
        let kf = P32E2::from(k);
        hi = x - kf * LN2_HI; /* k*ln2hi is exact here */
        lo = kf * LN2_LO;
        x = hi - lo;
    } else if hx > 0x_5ff_ffaa {
        /* |x| > 2**-14 */
        k = 0;
        hi = x;
        lo = ZERO;
    } else {
        return ONE + x;
    }

    /* x is now in primary range */
    let xx = x * x;
    let c = x - xx * (P1 + xx * P2);
    let y = ONE + (x * c / (TWO - c) - lo + hi);
    if k == 0 {
        y
    } else {
        scalbn::scalbn(y, k)
    }
}
