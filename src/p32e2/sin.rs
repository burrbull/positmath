use super::*;

const UP_1_PI_4: u32 = 0x_3c90_fdaa;
const UP_3_PI_4: u32 = 0x_496c_be40;
const UP_5_PI_4: u32 = 0x_4fb5_3d15;
const UP_7_PI_4: u32 = 0x_52fe_ddf5;
const UP_9_PI_4: u32 = 0x_5623_1d5f;

const PI_1_2: P32E2 = P32E2::new(0x_4490_fdaa);
const PI_2_2: P32E2 = P32E2::new(0x_4c90_fdaa);
const PI_3_2: P32E2 = P32E2::new(0x_516c_be40);
const PI_4_2: P32E2 = P32E2::new(0x_5490_fdaa);

#[inline]
pub fn sin(x: P32E2) -> P32E2 {
    /* sin(Inf or NaN) is NaN */
    if x.is_infinite() {
        return x;
    }

    let mut ix = x.to_bits();
    let sign = (ix >> 31) != 0;
    if sign {
        ix = ix.wrapping_neg()
    };

    if ix < UP_1_PI_4 {
        /* |x| ~<= pi/4 */
        if ix < 0x_800_0000 {
            /* |x| < 2**-12 */
            x
        } else {
            kernel::sin(x)
        }
    } else if ix <= UP_5_PI_4 {
        /* |x| ~<= 5*pi/4 */
        if ix < UP_3_PI_4 {
            /* |x| ~<= 3pi/4 */
            if sign {
                return -kernel::cos(x + PI_1_2);
            } else {
                return kernel::cos(x - PI_1_2);
            }
        } else {
            kernel::sin(if sign { -(x + PI_2_2) } else { -(x - PI_2_2) })
        }
    } else if ix < UP_9_PI_4 {
        /* |x| ~<= 9*pi/4 */
        if ix <= UP_7_PI_4 {
            /* |x| ~<= 7*pi/4 */
            if sign {
                kernel::cos(x + PI_3_2)
            } else {
                -kernel::cos(x - PI_3_2)
            }
        } else {
            kernel::sin(if sign { x + PI_4_2 } else { x - PI_4_2 })
        }
    } else {
        /* general argument reduction needed */
        let (n, y) = kernel::rem_pio2(x);
        match n & 3 {
            0 => kernel::sin(y),
            1 => kernel::cos(y),
            2 => kernel::sin(-y),
            _ => -kernel::cos(y),
        }
    }
}
