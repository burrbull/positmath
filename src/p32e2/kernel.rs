use super::*;

const ONE: P32E2 = P32E2::new(0x_4000_0000); // 1.
const MINUS_ONE: P32E2 = P32E2::new(-0x_4000_0000); // -1.

const C: [P32E2; 4] = [
    P32E2::new(-0x_37FF_FFFF), // -0.499999997251031003120
    P32E2::new(0x_1d55_54f8),  // 0.0416666233237390631894
    P32E2::new(-0x_0cd8_10FD), // -0.00138867637746099294692
    P32E2::new(0x_0499_342e),  // 0.0000243904487962774090654
];

/* |cos(x) - c(x)| < 2**-34.1 (~[-5.37e-11, 5.295e-11]). */
#[inline]
pub fn cos(x: P32E2) -> P32E2 {
    let z = x * x;
    let w = z * z;
    let r = z.mul_add(C[3], C[2]);
    (w * z).mul_add(r, w.mul_add(C[1], z.mul_add(C[0], ONE)))
}

const S: [P32E2; 4] = [
    P32E2::new(-0x_2aaa_aaaa), // -0.166666666416265235595
    P32E2::new(0x_1444_4422),  // 0.0083333293858894631756
    P32E2::new(-0x_07a0_0f9e), // -0.000198393348360966317347
    P32E2::new(0x_02b6_6c3c),  // 0.0000027183114939898219064
];

/* |sin(x)/x - s(x)| < 2**-37.5 (~[-4.89e-12, 4.824e-12]). */
#[inline]
pub fn sin(x: P32E2) -> P32E2 {
    let z = x * x;
    let w = z * z;
    let r = z.mul_add(S[3], S[2]);

    let s = z * x;
    (s * w).mul_add(r, z.mul_add(S[1], S[0]).mul_add(s, x))
}

const T: [P32E2; 6] = [
    P32E2::new(0x_32aa_a69a), // 0.333331395030791399758
    P32E2::new(0x_2889_7e9c), // 0.133392002712976742718
    P32E2::new(0x_1ed5_3247), // 0.0533812378445670393523
    P32E2::new(0x_1a47_7ce4), // 0.0245283181166547278873
    P32E2::new(0x_0f0b_b5c0), // 0.00297435743359967304927
    P32E2::new(0x_14d8_ae70), // 0.00946564784943673166728
];
/* |tan(x)/x - t(x)| < 2**-25.5 (~[-2e-08, 2e-08]). */
#[inline]
pub fn tan(x: P32E2, odd: bool) -> P32E2 {
    let z = x * x;
    let w = z * z;
    let s = z * x;
    let mut r = z.mul_add(T[5], T[4]);
    let t = z.mul_add(T[3], T[2]);
    let u = z.mul_add(T[1], T[0]);
    r = w.mul_add(r, t).mul_add(s * w, s.mul_add(u, x));
    if odd {
        MINUS_ONE / r
    } else {
        r
    }
}

const PIO2_1: P32E2 = P32E2::new(0x_4490_fda8); // 1.5707963109016418
const PIO2_1T: P32E2 = P32E2::new(0x_00c2_2169); // 0.000000015893256488652696

/// Return the remainder of x rem pi/2 in *y
///
/// use double precision for everything except passing x
/// use __rem_pio2_large() for large x
#[inline]
pub fn rem_pio2(x: P32E2) -> (i32, P32E2) {
    //let to_int = P32E2::from_bits(0x_4400_0000) / P32E2::epsilon();

    let mut ix = x.to_bits();
    let sign = (ix >> 31) != 0;
    if sign {
        ix = ix.wrapping_neg()
    };

    /* 25+53 bit pi is good enough for medium size */
    if ix < 0x_6e40_0000 {
        /* |x| ~<= 2^7*(pi/2), medium size */
        /* Use a specialized rint() to get fn.  Assume round-to-nearest. */
        let f_n = (x * p32e2::consts::FRAC_2_PI).round() /*+ to_int - to_int*/;
        return (i32::from(f_n), x - f_n * PIO2_1 - f_n * PIO2_1T);
    }

    if x.is_infinite() {
        return (0, x);
    }

    unimplemented!()

    /* /* scale x into [2^23, 2^24-1] */
    let mut tx: [f64; 1] = [0.];
    let e0 = ((ix >> 23) - (0x7f + 23)) as i32; /* e0 = ilogb(|x|)-23, positive */
    tx[0] = f32::from_bits(ix - (e0 << 23) as u32) as f64;
    let (n, ty) = rem_pio2_large(&tx, e0, 0);
    if sign {
    return (-n, -ty[0]);
    }
    (n, ty[0])
     */
}
