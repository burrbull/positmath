use super::*;

const X1P110: P32E2 = P32E2::new(0x_7fff_fffa);

#[inline]
pub fn scalbn(mut x: P32E2, mut n: i32) -> P32E2 {
    let sign = (n >> 31) != 0;
    if sign {
        n = -n;
    }

    if n > 110 {
        if sign {
            x /= X1P110;
        } else {
            x *= X1P110;
        }
        n -= 110;
        if n > 110 {
            if sign {
                x /= X1P110;
            } else {
                x *= X1P110;
            }
            n -= 110;
        }
    }

    let k = n >> 2;

    let exp_a: u32 = ((n & 0x3) as u32) << (27 - k);

    let ui_a = (0x7FFF_FFFF ^ (0x3FFF_FFFF >> k)) | exp_a;

    if sign {
        x / P32E2::from_bits(ui_a)
    } else {
        x * P32E2::from_bits(ui_a)
    }
}
