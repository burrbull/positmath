#![allow(unused_imports)]
#![allow(dead_code)]

use crate::p32e2::*;

use crate::MathExt;
#[cfg(feature = "test")]
use libm::F64Ext;

const U_1: P32E2 = P32E2::new(0x_4000_0000);
const U_35: P32E2 = P32E2::new(0x_4e00_0000);

fn ulp(x: P32E2) -> P32E2 {
    let t = x.abs();
    let near = P32E2::from_bits(t.to_bits() - 1);
    (t - near).abs()
}

#[test]
fn cos() {
    let xp = P32E2::new(0x_3c00_0000);
    let x64 = f64::from(xp);
    let correct = P32E2::from(x64.cos());
    assert!((xp.cos() - correct).abs() < correct.abs());
}

#[test]
fn exp() {
    use rand::Rng;
    let mut rng = rand::thread_rng();
    for _ in 0..1000 {
        let xpb: i32 = rng.gen_range(-0x_6930_0000, 0x_6930_0000);
        //let xpb : i32 = rng.gen_range(-0x_68cf_0785, 0x_68cf_0785);
        let xp = P32E2::new(xpb);
        let x64 = f64::from(xp);
        let correct = P32E2::from(x64.exp());
        let answer = xp.exp();
        assert!(
            (answer - correct).abs() < correct.abs(),
            "x = {}, answer = {}, correct = {}",
            x64,
            f64::from(answer),
            f64::from(correct)
        );
    }
}

#[test]
fn scalbn() {
    use rand::Rng;
    let mut rng = rand::thread_rng();
    for _ in 0..1000 {
        let xpb: i32 = rng.gen_range(-0x_6930_0000, 0x_6930_0000);
        let n: i32 = rng.gen_range(-200, 200);
        let xp = P32E2::new(xpb);
        let x64 = f64::from(xp);
        let correct = P32E2::from(libm::scalbn(x64, n as i32));
        let answer = self::scalbn::scalbn(xp, n);
        assert!(
            (answer - correct).abs() < correct.abs(),
            "x = {}, n = {}, answer = {}, correct = {}",
            x64,
            n,
            f64::from(answer),
            f64::from(correct)
        );
    }
}
