#![no_std]
#![feature(test)]

extern crate test;

pub mod p32e2;

pub trait MathExt {
    fn cos(self) -> Self;
    fn sin(self) -> Self;
    fn tan(self) -> Self;
    fn exp(self) -> Self;
}
