pub use softposit::{p32e2, P32E2};

mod kernel;

pub mod cos;
pub mod exp;
pub mod scalbn;
pub mod sin;
pub mod tan;

impl crate::MathExt for P32E2 {
    #[inline]
    fn cos(self) -> Self {
        self::cos::cos(self)
    }
    #[inline]
    fn sin(self) -> Self {
        self::sin::sin(self)
    }
    #[inline]
    fn tan(self) -> Self {
        self::tan::tan(self)
    }
    #[inline]
    fn exp(self) -> Self {
        self::exp::exp(self)
    }
}

mod tests;
